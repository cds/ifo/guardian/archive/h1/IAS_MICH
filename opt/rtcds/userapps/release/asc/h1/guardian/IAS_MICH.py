# IAS_MICH.py
#
# $Id: IAS_MICH.py 9158 2014-11-18 23:27:56Z alexan.staley@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/asc/h1/guardian/IAS_MICH.py $
#

from LSC_CONFIGS import\
				DOWN,\
				MICH_SET,\
				LOCK_MICH_DARK,\
				MICH_DARK_LOCKED

##################################################

edges = [
# MMICH edges:
    ('DOWN', 'MICH_SET'),
    ('MICH_SET', 'LOCK_MICH_DARK'),
    ('LOCK_MICH_DARK', 'MICH_DARK_LOCKED'),
]
