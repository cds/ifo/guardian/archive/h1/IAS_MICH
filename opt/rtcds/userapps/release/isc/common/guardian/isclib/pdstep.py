class Step_prcl_9I_27I(object):
    def __init__(self):
        tstep = 0.01
		# before the LSC model split
        #chan0 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_3_20', '+0.034, 100', tstep)
        #chan1 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_3_6', '-0.01, 100', tstep)
        chan0 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_4_17', '+0.034, 100', tstep)
        chan1 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_4_5', '-0.01, 100', tstep)
    def step(self):
        chan0.step()
        chan1.step()

class Step_prcl_27I_9I(object):
    def __init__(self):
        tstep = 0.01
		# before the LSC model split
        #chan0 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_3_20', '-0.034, 100', tstep)
        #chan1 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_3_6', '+0.01, 100', tstep)
        chan0 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_4_17', '-0.034, 100', tstep)
        chan1 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_4_5', '+0.01, 100', tstep)
    def step(self):
        chan0.step()
        chan1.step()

class Step_mich_45Q_135Q(object):
    def __init__(self):
        tstep = 0.01
		# before the LSC model split
        #chan0 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_2_23', '+0.126, 100', tstep)
        #chan1 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_2_9', '-0.01, 100', step)
        chan0 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_3_20', '+0.126, 100', tstep)
        chan1 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_3_8', '-0.01, 100', step)
    def step(self):
        chan0.step()
        chan1.step()

class Step_mich_135Q_45Q(object):
    def __init__(self):
        tstep = 0.01
		# before the LSC model split
        #chan0 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_2_23', '-0.126, 100', tstep)
        #chan1 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_2_9', '+0.01, 100', step)
        chan0 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_3_20', '-0.126, 100', tstep)
        chan1 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_3_8', '+0.01, 100', step)
    def step(self):
        chan0.step()
        chan1.step()

class Step_srcl_45I_135I(object):
    def __init__(self):
        tstep = 0.01
		# before the LSC model split
        #chan0 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_4_22', '+0.12,100', tstep)
        #chan1 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_4_20', '+0.032,100', tstep)
        #chan2 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_4_8', '-0.01, 100', tstep)
        #chan3 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_4_6', '-0.0042, 100', tstep)
        chan0 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_5_19', '+0.12,100', tstep)
        chan1 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_5_17', '+0.032,100', tstep)
        chan2 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_5_7', '-0.01, 100', tstep)
        chan3 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_5_6', '-0.0042, 100', tstep)
    def step(self):
        chan0.step()
        chan1.step()
        chan2.step()
        chan3.step()

class Step_srcl_45I_135I(object):
    def __init__(self):
        tstep = 0.01
		# before the LSC model split
        #chan0 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_4_22', '+0.12,100', tstep)
        #chan1 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_4_20', '+0.032,100', tstep)
        #chan2 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_4_8', '-0.01, 100', tstep)
        #chan3 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_4_6', '-0.0042, 100', tstep)
        chan0 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_5_19', '-0.12,100', tstep)
        chan1 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_5_17', '-0.032,100', tstep)
        chan2 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_5_7', '+0.01, 100', tstep)
        chan3 = cdsutils.Step(ezca, 'LSC-PD_DOF_MTRX_5_6', '+0.0042, 100', tstep)
    def step(self):
        chan0.step()
        chan1.step()
        chan2.step()
        chan3.step()
